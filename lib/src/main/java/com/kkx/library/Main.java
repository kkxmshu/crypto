package com.kkx.library;

import com.kkx.library.cipher.Binary;

import java.io.IOException;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 03/12/16
 */
public class Main {

    public static void main(String[] args) throws IOException {
//        final Cipher gamma = new Gamma("LEMON");
//        final String text = "Hello, World";
//        final String encrypted = gamma.encrypt(text);
//
//        System.out.println(encrypted);
//        final String decrypted = gamma.decrypt(encrypted);
//        System.out.println(decrypted);
//
//        System.out.println("---------");
//        final Cipher vigenere = new Vigenere("LEMON");
//        final String encrypted1 = vigenere.encrypt(text);
//
//        System.out.println(encrypted1);
//        final String decrypted1 = vigenere.decrypt(encrypted1);
//        System.out.println(decrypted1);

//        Lab1_1.main("input.txt", "output.txt", "false", "41p5tiv080fgja222rp2c9nkkb");
//        Lab1_1.main("output.txt", "decrypt.txt", "true", "41p5tiv080fgja222rp2c9nkkb");

//        Lab1_2.main("input.txt", "output.txt", "false", "474");
//        Lab1_2.main("output.txt", "decrypted.txt", "false", "474");

//        Lab2.main("input.txt", "output.txt", "12", "false");
//        Lab2.main("output.txt", "decrypted.txt", "12", "true");

//        final Cipher magicSquare = new MagicSquare();
//        String encrypt = magicSquare.encrypt("DURING the whole of");
//        System.out.println("encrypted: " + encrypt);
//        System.out.println("decrypted: " + magicSquare.decrypt(encrypt));
//
        final Binary binary = new Binary(7321);
        String encrypt = binary.encrypt("14286035");
        System.out.println(encrypt);

//        BigInteger generate = Generator.generate(new BigInteger("10"));
//        System.out.println(generate);
    }
}