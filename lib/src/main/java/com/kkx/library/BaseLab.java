package com.kkx.library;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 12/12/16
 */
abstract class BaseLab {

    protected static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
}