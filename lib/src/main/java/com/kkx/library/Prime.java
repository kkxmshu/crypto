package com.kkx.library;

import java.math.BigInteger;
import java.util.Random;

public class Prime {

    public static void main(String[] args) {
//        long[] numbers = {1000L};
//
//        System.out.println("Тест Фермана");
//        for (long number : numbers) {
//            checkFerman(number);
//        }
//
//        System.out.println();
//        System.out.println("Тест Соловея-Штрассена");
//        for (long number : numbers) {
//            checkSolovey(number);
//        }
//
//        System.out.println();
//        System.out.println("Тест Миллера-Рабина");
//        for (long number : numbers) {
//            checkMillerRabin(number);
//        }

        for (int i = 0; i < 20; i++) {
            final int number = 10;
//                    new Eratosfen().getn();
            System.out.println("сгенерированное число: " + number);
            System.out.println();
            System.out.println("Тест Фермана");
            checkFerman(number);

            System.out.println();
            System.out.println("Тест Соловея-Штрассена");
            checkSolovey(number);

            System.out.println();
            System.out.println("Тест Миллера-Рабина");
            checkMillerRabin(number);

            try {
                Thread.sleep(500L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void checkFerman(long n) {
        System.out.println(checkPrimeFerman(n, 20));
    }
    public static void checkSolovey(long n) {
        System.out.println(is(n, isPrimeSolovay(n, 20)));

    } public static void checkMillerRabin(long n) {
//        System.out.println(is(n, new MillerRabin().isPrime(n, 20)));
    }

    public static String is(long n, boolean is) {
        return n + " " + (is ? "простое" : "составное");
    }

    private final static Random rand = new Random();

    private static BigInteger getRandomFermatBase(BigInteger n) {
        // Rejection method: ask for a random integer but reject it if it isn't
        // in the acceptable set.

        while (true) {
            final BigInteger a = new BigInteger(n.bitLength(), rand);
            // must have 1 <= a < n
            if (BigInteger.ONE.compareTo(a) <= 0 && a.compareTo(n) < 0) {
                return a;
            }
        }
    }

    public static String checkPrimeFerman(Long n, int maxIterations) {
        return is(n, checkPrime(BigInteger.valueOf(n), maxIterations));
    }

    public static boolean checkPrime(BigInteger n, int maxIterations) {
        if (n.equals(BigInteger.ONE))
            return false;

        for (int i = 0; i < maxIterations; i++) {
            BigInteger a = getRandomFermatBase(n);
            a = a.modPow(n.subtract(BigInteger.ONE), n);

            if (!a.equals(BigInteger.ONE))
                return false;
        }

        return true;
    }


    /** Function to calculate jacobi (a/b) **/
    public static long Jacobi(long a, long b)
    {
        if (b <= 0 || b % 2 == 0)
            return 0;
        long j = 1L;
        if (a < 0)
        {
            a = -a;
            if (b % 4 == 3)
                j = -j;
        }
        while (a != 0)
        {
            while (a % 2 == 0)
            {
                a /= 2;
                if (b % 8 == 3 || b % 8 == 5)
                    j = -j;
            }

            long temp = a;
            a = b;
            b = temp;

            if (a % 4 == 3 && b % 4 == 3)
                j = -j;
            a %= b;
        }
        if (b == 1)
            return j;
        return 0;
    }
    /** Function to checkFerman if prime or not **/
    public static boolean isPrimeSolovay(long n, int iteration)
    {
        /** base case **/
        if (n == 0 || n == 1)
            return false;
        /** base case - 2 is prime **/
        if (n == 2)
            return true;
        /** an even number other than 2 is composite **/
        if (n % 2 == 0)
            return false;

        Random rand = new Random();
        for (int i = 0; i < iteration; i++)
        {
            long r = Math.abs(rand.nextLong());
            long a = r % (n - 1) + 1;
            long jacobian = (n + Jacobi(a, n)) % n;
            long mod = modPow(a, (n - 1)/2, n);
            if(jacobian == 0 || mod != jacobian)
                return false;
        }
        return true;
    }
    /** Function to calculate (a ^ b) % c **/
    public static long modPow(long a, long b, long c)
    {
        long res = 1;
        for (int i = 0; i < b; i++)
        {
            res *= a;
            res %= c;
        }
        return res % c;
    }

}