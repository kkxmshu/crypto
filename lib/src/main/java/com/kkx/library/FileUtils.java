package com.kkx.library;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 08.12.16
 */
public class FileUtils {

    public static String readFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    public static boolean deleteFile(String path) throws IOException {
        return Files.deleteIfExists(Paths.get(path));
    }

    public static boolean writeFile(String filename, String text) throws IOException {
        return Files.write(Paths.get(filename), text.getBytes(), StandardOpenOption.CREATE) != null;
    }
}
