package com.kkx.library.cipher;

import java.util.Arrays;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 12/12/16
 */
public class Feistel implements Cipher {

    private int mRounds;

    public Feistel(int rounds) {
        mRounds = rounds;
    }

    @Override
    public String encrypt(String text) {
        final int half = text.length() / 2 + (text.length() % 2);
        char[] x1 = text.substring(0, half).toCharArray();
        char[] x2 = fill(text.substring(half), half);

        char[] buffer;
        for (int i = 1; i <= mRounds; i++) {
            buffer = x2;
            x2 = x1;
            x1 = xor(buffer, f(x1, i));
        }

        return new String(x1) + new String(x2);
    }

    @Override
    public String decrypt(String encrypted) {
        if (encrypted.length() % 2 != 0) {
            throw new UnknownError();
        }
        char[] x1 = encrypted.substring(0, encrypted.length() / 2).toCharArray();
        char[] x2 = encrypted.substring(encrypted.length() / 2).toCharArray();

        char[] buffer;
        for (int i = mRounds; i > 0; i--) {
            buffer = x1;
            x1 = x2;
            x2 = xor(buffer, f(x2, i));
        }

        return new String(x1) + new String(x2);
    }

    private static char[] fill(String text, int half) {
        final char[] chars = new char[half];
        for (int i = 0; i < text.length(); i++) {
            chars[i] = text.charAt(i);
        }
        Arrays.fill(chars, text.length(), half, '\0');
        return chars;
    }

    private static char[] f(char x[], int r) {
        final char chars[] = new char[x.length];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char) (x[i] + r);
        }
        return chars;
    }

    private static char[] xor(char x1[], char x2[]) {
        final char chars[] = new char[x1.length];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char) (x1[i] ^ x2[i]);
        }
        return chars;
    }
}