package com.kkx.library.cipher;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 03/12/16
 */
public class Vernam implements Cipher {

    private String mKey;

    public Vernam(String key) {
        mKey = key;
    }

    @Override
    public String encrypt(String text) {
        final int length = text.length();
        if (length <= mKey.length()) {
            final char[] chars = new char[length];
            for (int i = 0; i < length; i++) {
                chars[i] = (char) (text.charAt(i) ^ mKey.charAt(i));
            }
            return new String(chars);

        } else {
            throw new IndexOutOfBoundsException("Text's length must be less or equals to key's length.");
        }
    }

    @Override
    public String decrypt(String encrypted) {
        return encrypt(encrypted);
    }

    public void setKey(String key) {
        mKey = key;
    }
}