package com.kkx.library.cipher;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 03/12/16
 */
public class Caesar implements Cipher, Changable {

    private Dictionary mDictionary;
    private int mShift;

    public Caesar(Dictionary dictionary, int shift) {
        if (shift >= 0 && shift < dictionary.length()) {
            mDictionary = dictionary;
            mShift = shift;

        } else {
            throw new IndexOutOfBoundsException("Shift must be in range: [0.." + dictionary.length() + ")");
        }
    }

    public Caesar(int shift) {
        this(Dictionary.LATIN, shift);
    }

    @Override
    public String encrypt(String text) {
        return shift(text, mShift, mDictionary);
    }

    @Override
    public String decrypt(String encrypted) {
        return shift(encrypted, -mShift, mDictionary);
    }

    private static String shift(String text, int shift, Dictionary dictionary) {
        final char[] chars = new char[text.length()];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = shift(text.charAt(i), shift, dictionary);
        }
        return new String(chars);
    }

    private static char shift(char ch, int shift, Dictionary dictionary) {
        final int index = dictionary.indexOf(ch);
        final int length = dictionary.length();

        return index < 0 ? Dictionary.UNSUPPORTED : dictionary.charAt((length + index + shift) % length);
    }

    @Override
    public void setDictionary(Dictionary dictionary) {
        mDictionary = dictionary;
    }

    public void setShift(int shift) {
        mShift = shift;
    }
}