package com.kkx.library.cipher;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 10.12.16
 */
public class Scrambler implements Cipher {

    private int mSeed;
    private boolean mSecondary;

    public Scrambler(int seed, boolean secondary) {
        mSeed = seed;
        mSecondary = secondary;
    }

    @Override
    public String encrypt(String text) {
        final char[] chars = new char[text.length()];
        for (int i = 0; i < chars.length; i++) {
            step();
            chars[i] = (char) (text.charAt(i) ^ mSeed);
        }
        return new String(chars);
    }

    @Override
    public String decrypt(String encrypted) {
        return encrypt(encrypted);
    }

    public int step() {
        // x8 + x5 + x3 + x2 + 1
        // x8 + x7 + x6 + x3 + x2 + 1

        mSeed = mSecondary ?
                ((((mSeed >> 5) ^ (mSeed >> 3) ^ (mSeed >> 2)) & 0x01) << 5) | (mSeed >> 1) :
                ((((mSeed >> 7) ^ (mSeed >> 6) ^ (mSeed >> 3) ^ (mSeed >> 2)) & 0x01) << 7) | (mSeed >> 1);

        return mSeed & 0x01;
    }

    public void setSeed(int seed) {
        mSeed = seed;
    }

    @Override
    public String toString() {
        return mSeed + "";
    }
}
