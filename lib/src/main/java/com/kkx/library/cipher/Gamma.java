package com.kkx.library.cipher;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 08.12.16
 * @deprecated the same as {@link Vigenere} :C
 */
public class Gamma implements Cipher, Changable {

    private Dictionary mDictionary;
    private String mGamma;

    public Gamma(Dictionary dictionary, String gamma) {
        mDictionary = dictionary;
        mGamma = gamma;
    }

    public Gamma(String gamma) {
        this(Dictionary.LATIN, gamma);
    }

    @Override
    public String encrypt(String text) {
        return gamma(text, 1, 0);
    }

    @Override
    public String decrypt(String encrypted) {
        return gamma(encrypted, -1, 1);
    }

    private String gamma(String text, int arg1, int arg2) {
        final char[] chars = new char[text.length()];
        final int length = mDictionary.length();
        for (int i = 0; i < text.length(); i++) {
            int charIndex = mDictionary.indexOf(text.charAt(i));
            int keyIndex = mDictionary.indexOf(mGamma.charAt(i % mGamma.length()));
            chars[i] = mDictionary.charAt((charIndex + arg1 * keyIndex + arg2 * length) % length);
        }
        return new String(chars);
    }

    @Override
    public void setDictionary(Dictionary dictionary) {
        mDictionary = dictionary;
    }
}
