package com.kkx.library.cipher;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 03/12/16
 */
public class Vigenere implements Cipher, Changable {

    private Dictionary mDictionary;
    private String mKey;

    public Vigenere(Dictionary dictionary, String key) {
        mDictionary = dictionary;
        mKey = key;
    }

    public Vigenere(String key) {
        this(Dictionary.LATIN, key);
    }

    @Override
    public String encrypt(String text) {
        return shift(text, mKey, 1, mDictionary);
    }

    @Override
    public String decrypt(String encrypted) {
        return shift(encrypted, mKey, -1, mDictionary);
    }

    private static String shift(String text, String key, int factor, Dictionary dictionary) {
        final char[] chars = new char[text.length()];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = shift(text.charAt(i), key, i, factor, dictionary);
        }
        return new String(chars);
    }

    private static char shift(char ch, String key, int position, int factor, Dictionary dictionary) {
        final char keyChar = key.charAt(position % key.length());

        final int textShift = dictionary.indexOf(ch);
        final int keyShift = dictionary.indexOf(keyChar);
        final int length = dictionary.length();

        return textShift < 0 || keyShift < 0 ? Dictionary.UNSUPPORTED : dictionary.charAt((length + textShift + factor * keyShift) % length);
    }

    @Override
    public void setDictionary(Dictionary dictionary) {
        mDictionary = dictionary;
    }

    public void setKey(String key) {
        mKey = key;
    }
}