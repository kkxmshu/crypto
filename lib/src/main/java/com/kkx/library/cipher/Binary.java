package com.kkx.library.cipher;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 21/12/16
 */
public class Binary implements Cipher {

    private static final char ZERO = '0';
    private static final char ONE = '1';
    private static final char SPACE = ' ';

    private String mMask;

    public Binary(int mask) {
        mMask = String.valueOf(mask);
    }

    public String encrypt(int value) {
        return encrypt(String.valueOf(value));
    }

    @Override
    public String encrypt(String text) {
        final char[] mask = mMask.toCharArray();
        final char[] result = new char[mask.length * text.length() + text.length()];
        int i = 0;
        int aInt, bInt;
        for (char a : text.toCharArray()) {
            aInt = a - ZERO;
            int rest = -1;
            for (char b : mask) {
                bInt = b - ZERO;
                if (rest == -1 && aInt >= bInt) {
                    result[i] = ONE;
                    rest = aInt - bInt;

                } else if (rest != -1) {
                    result[i] = rest >= bInt ? ONE : ZERO;
                    rest = rest - bInt >= 0 ? (rest - bInt) : rest;

                } else {
                    result[i] = ZERO;
                }
                i++;
            }
            System.out.println();
            result[i] = SPACE;
            i++;
        }
        return new String(result);
    }

    @Override
    public String decrypt(String encrypted) {
        throw new NoSuchMethodError();
    }
}
