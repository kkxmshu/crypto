package com.kkx.library.cipher;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 04.12.16
 */
public interface Changable {
    void setDictionary(Dictionary dictionary);
}
