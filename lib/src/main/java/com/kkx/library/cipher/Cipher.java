package com.kkx.library.cipher;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 03/12/16
 */
public interface Cipher {

    String encrypt(String text);
    String decrypt(String encrypted);
}