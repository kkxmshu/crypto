package com.kkx.library.cipher;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 18/12/16
 */
public class MagicSquare implements Cipher {

    private int[][] mSquare;

    @Override
    public String encrypt(String text) {
        if (mSquare == null) {
            double size = Math.sqrt(text.length());
            int sizeInt = (int) size;
            if (size - sizeInt != 0) {
                sizeInt++;
            }
            mSquare = getMagicSquare(sizeInt);
        }

        // Помещает символ исходного текста в соответствующую позицию в магическом квадрате
        final int[] flat = toFlat(mSquare);
        final char[] chars = new char[flat.length];
        for (int i = 0; i < flat.length; i++) {
            if (flat[i] - 1 < text.length()) {
                chars[i] = text.charAt(flat[i] - 1);
            } else {
                // Заполняет оставшиеся ячейки квадрата символами '\0'
                chars[i] = Dictionary.UNSUPPORTED;
            }
        }
        return new String(chars);
    }

    @Override
    public String decrypt(String encrypted) {
        if (mSquare == null) {
            throw new IndexOutOfBoundsException("Square must not be empty!");
        }

        final int[] flat = toFlat(mSquare);
        final char[] chars = new char[flat.length];
        int j = 0;
        for (int i : flat) {
            chars[i - 1] = encrypted.charAt(j++);
        }
        return new String(chars);
    }

    // Генерирует магический квадрат размерностью n. n должно быть нечетным числом
    public static int[][] getMagicSquare(int n) {
        if (n % 2 == 0) n++;
        int[][] square = new int[n][n];

        int row = n - 1;
        int col = n / 2;
        square[row][col] = 1;

        for (int i = 2; i <= n * n; i++) {
            if (square[(row + 1) % n][(col + 1) % n] == 0) {
                row = (row + 1) % n;
                col = (col + 1) % n;

            } else {
                row = (row - 1 + n) % n;
            }
            square[row][col] = i;
        }
        System.out.println();
        printSquare(square);
        System.out.println();
        return square;
    }

    public static void printSquare(int[][] square) {
        for (int[] row : square) {
            for (int col : row) {
                if (col < 10) System.out.print(" ");  // for alignment
                if (col < 100) System.out.print(" ");  // for alignment
                System.out.print(col + " ");
            }
            System.out.println();
        }
    }

    // Преобразование 2d-массива в 1d
    private static int[] toFlat(int[][] square) {
        int i = 0;
        final int[] flat = new int[square.length * square[0].length];
        for (int[] row : square) {
            for (int col : row) {
                flat[i++] = col;
            }
        }
        return flat;
    }
}
