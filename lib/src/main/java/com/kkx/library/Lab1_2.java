package com.kkx.library;

import com.kkx.library.cipher.Cipher;
import com.kkx.library.cipher.Scrambler;

import java.io.IOException;
import java.util.Random;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 12/12/16
 */
public class Lab1_2 extends BaseLab {

    private static final String VALUE_FILENAME = "value.txt";

    public static void main(String... args) {
        final String input = args[0];
        final String output = args[1];

        boolean secondary = false;
        int value = 0;

        switch (args.length) {
            case 4:
                value = Integer.parseInt(args[3]);
            case 3:
                secondary = Boolean.parseBoolean(args[2]);
            default:
                break;
        }

        if (!isEmpty(input) && !isEmpty(output)) {
            try {
                final String text = FileUtils.readFile(input);
                final Cipher cipher = new Scrambler(value != 0 ? value : (value = randomValue()), secondary);

                FileUtils.deleteFile(output);
                FileUtils.writeFile(output, cipher.encrypt(text));

                FileUtils.deleteFile(VALUE_FILENAME);
                FileUtils.writeFile(VALUE_FILENAME, String.valueOf(value));

                System.out.println("File \"" + input + "\" successfully encrypted into \"" + output + "\"\n"
                        + "Initial value saved in " + VALUE_FILENAME + " [" + value + "]\n");

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            throw new IllegalArgumentException("Program arguments must be in " +
                    "following sequence: input output secondary [boolean] value " +
                    "(e.g. \"input.txt output.txt false 200 (not required)\")");
        }
    }

    private static int randomValue() {
        return new Random().nextInt((1000) + 1);
    }
}