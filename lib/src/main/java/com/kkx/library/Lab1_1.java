package com.kkx.library;

import com.kkx.library.cipher.Cipher;
import com.kkx.library.cipher.Dictionary;
import com.kkx.library.cipher.Vigenere;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 08.12.16
 */
public class Lab1_1 extends BaseLab {

    private static final String GAMMA_FILENAME = "gamma.txt";

    public static void main(String... args) {
        final String input = args[0];
        final String output = args[1];
        final boolean decrypt = args.length > 2 && Boolean.parseBoolean(args[2]);
        String gamma = args.length > 3 ? args[3] : null;

        if (!isEmpty(input) && !isEmpty(output)) {
            try {
                final String text = FileUtils.readFile(input);
                final Cipher cipher = new Vigenere(Dictionary.MIXED, isEmpty(gamma) ?
                        gamma = randomGamma() : gamma);

                FileUtils.deleteFile(output);
                FileUtils.writeFile(output, decrypt ? cipher.decrypt(text) : cipher.encrypt(text));

                FileUtils.deleteFile(GAMMA_FILENAME);
                FileUtils.writeFile(GAMMA_FILENAME, gamma);

                System.out.println("File \"" + input + "\" successfully " +
                        (decrypt ? "decrypted" : "encrypted") + " into \"" + output + "\"\n" +
                        "Gamma saved in " + GAMMA_FILENAME + " [" + gamma + "]");

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            throw new IllegalArgumentException("Program arguments must be in " +
                    "following sequence: input output decrypt [boolean] gamma " +
                    "(e.g. \"input.txt output.txt false lemon (not required)\")");
        }
    }

    private static String randomGamma() {
        return new BigInteger(130, new Random()).toString(32);
    }
}
