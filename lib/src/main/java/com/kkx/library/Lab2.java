package com.kkx.library;

import com.kkx.library.cipher.Cipher;
import com.kkx.library.cipher.Feistel;

import java.io.IOException;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 12/12/16
 */
public class Lab2 extends BaseLab {

    public static void main(String... args) {
        final String input = args[0];
        final String output = args[1];

        final int raunds = Integer.parseInt(args[2]);
        final boolean decrypt = Boolean.parseBoolean(args[3]);

        if (!isEmpty(input) && !isEmpty(output)) {
            try {
                final String text = FileUtils.readFile(input);
                final Cipher cipher = new Feistel(raunds);

                FileUtils.deleteFile(output);
                FileUtils.writeFile(output, decrypt ? cipher.decrypt(text) : cipher.encrypt(text));

                System.out.println("File \"" + input + "\" successfully encrypted into \"" + output + "\"\n");

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            throw new IllegalArgumentException("Program arguments must be in " +
                    "following sequence: input output raunds decrypt [boolean] " +
                    "(e.g. \"input.txt output.txt 12 false\")");
        }
    }
}