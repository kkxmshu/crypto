package com.kkx.crypto.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.kkx.crypto.R;
import com.kkx.crypto.Utils;
import com.kkx.crypto.interfaces.ObjectsReceiver;
import com.kkx.library.cipher.Caesar;
import com.kkx.library.cipher.Cipher;
import com.kkx.library.cipher.Dictionary;
import com.kkx.library.cipher.Vernam;
import com.kkx.library.cipher.Vigenere;

import java.lang.ref.WeakReference;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 04.12.16
 */
public class ConfigView extends RelativeLayout
        implements RadioGroup.OnCheckedChangeListener, View.OnClickListener
        , SeekBar.OnSeekBarChangeListener, TextWatcher {

    private WeakReference<ObjectsReceiver> mReceiver;

    private TextView mTvTitle;
    private ImageView mIvArrow;

    private LinearLayout mExpandedLayout;

    private TextView mTvDictionary;
    private RadioGroup mRgCipher;
    private RadioGroup mRgDictionary;

    private TextView mTvKeyShift;
    private SeekBar mSeekBar;
    private EditText mEtKey;

    private final String mFormatShift;

    public ConfigView(@NonNull Context context) {
        this(context, null);
    }

    public ConfigView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ConfigView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_config, this);

        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvArrow = (ImageView) findViewById(R.id.iv_arrow);
        mExpandedLayout = (LinearLayout) findViewById(R.id.ll_expanded);
        mTvDictionary = (TextView) findViewById(R.id.tv_dictionary);
        mRgCipher = (RadioGroup) findViewById(R.id.rg_cipher);
        mRgDictionary = (RadioGroup) findViewById(R.id.rg_dictionary);
        mTvKeyShift = (TextView) findViewById(R.id.tv_key_shift);
        mSeekBar = (SeekBar) findViewById(R.id.sb_shift);
        mEtKey = (EditText) findViewById(R.id.et_key);

        mRgCipher.setOnCheckedChangeListener(this);
        mRgDictionary.setOnCheckedChangeListener(this);
        mSeekBar.setOnSeekBarChangeListener(this);
        mEtKey.addTextChangedListener(this);

        mFormatShift = context.getString(R.string.format_shift_d);

        check();

        super.setOnClickListener(this);
    }

    public void setReceiver(@NonNull ObjectsReceiver receiver) {
        mReceiver = new WeakReference<>(receiver);
    }

    public int getKeyLength() {
        return mEtKey.getText().toString().trim().length();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        check();
        final int groupId = radioGroup.getId();
        Utils.receiveObjects(mReceiver, R.id.code_checked_changed, groupId, i,
                groupId == R.id.rg_cipher ? createCipher(i) : null);
    }

    private Cipher createCipher(int buttonId) {
        final Dictionary dictionary = Dictionary.values()[Utils.getDictionaryIndex(
                mRgDictionary.getCheckedRadioButtonId()
        )];
        switch (buttonId) {
            case R.id.rb_caesar:
                return new Caesar(dictionary, mSeekBar.getProgress());

            case R.id.rb_vigenere:
                return new Vigenere(dictionary, mEtKey.getText().toString());

            case R.id.rb_vernam:
                return new Vernam(mEtKey.getText().toString());

            default:
                return null;
        }
    }

    private void check() {
        final int cipherRes = getRadioButtonText(mRgCipher.getCheckedRadioButtonId());
        if (cipherRes == R.string.vernam) {
            mTvKeyShift.setText(R.string.key);
            Utils.changeVisibility(VISIBLE, mEtKey, mTvKeyShift);
            Utils.changeVisibility(GONE, mSeekBar, mRgDictionary, mTvDictionary);
            return;

        } else if (cipherRes == R.string.vigenere) {
            mTvKeyShift.setText(R.string.key);
            Utils.changeVisibility(VISIBLE, mEtKey, mTvKeyShift);
            Utils.changeVisibility(GONE, mSeekBar);

        } else {
            mTvKeyShift.setText(String.format(mFormatShift, mSeekBar.getProgress()));
            Utils.changeVisibility(VISIBLE, mSeekBar, mTvKeyShift);
            Utils.changeVisibility(GONE, mEtKey);
        }

        Utils.changeVisibility(VISIBLE, mRgDictionary, mTvDictionary);
    }

    @StringRes
    private static int getRadioButtonText(int id) {
        switch (id) {
            case R.id.rb_caesar:
                return R.string.caesar;

            case R.id.rb_vigenere:
                return R.string.vigenere;

            case R.id.rb_vernam:
                return R.string.vernam;

            case R.id.rb_latin:
                return R.string.latin;

            case R.id.rb_cyrillic:
                return R.string.cyrillic;

            case R.id.rb_mixed:
                return R.string.mixed;

            default:
                return 0;
        }
    }

    private void updateTitle() {
        final int cipherIdRes = getRadioButtonText(mRgCipher.getCheckedRadioButtonId());
        final int dictionaryIdRes = getRadioButtonText(mRgDictionary.getCheckedRadioButtonId());

        final String third;
        if (cipherIdRes == R.string.caesar) {
            third = String.valueOf(mSeekBar.getProgress());

        } else {
            third = mEtKey.getText().toString().trim();
            if (cipherIdRes == R.string.vernam) {
                mTvTitle.setText(Utils.beautify(getContext(), third, cipherIdRes));
                return;
            }
        }

        mTvTitle.setText(Utils.beautify(getContext(), third, cipherIdRes, dictionaryIdRes));
    }

    @Override
    public void onClick(View view) {
        final boolean visible = mExpandedLayout.getVisibility() == VISIBLE;
        if (visible) {
            updateTitle();
        }
        mExpandedLayout.setVisibility(visible ? GONE : VISIBLE);
        mTvTitle.setVisibility(visible ? VISIBLE : INVISIBLE);
        rotate(mIvArrow).start();
    }

    public Animator rotate(@NonNull View view) {
        final ObjectAnimator animator = ObjectAnimator.ofFloat(view, View.ROTATION,
                view.getRotation() + 180);

        animator.setDuration(200);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addListener(mAnimatorListener);
        return animator;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        mTvKeyShift.setText(String.format(mFormatShift, i));
        Utils.receiveObjects(mReceiver, R.id.code_seekbar_changed, i);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        final String text = editable.toString();
        if (!TextUtils.isEmpty(text)) {
            Utils.receiveObjects(mReceiver, R.id.code_key_changed, text);
        }
    }

    private final Animator.AnimatorListener mAnimatorListener = new Animator.AnimatorListener() {

        @Override
        public void onAnimationStart(Animator animator) {
            ConfigView.this.setEnabled(false);
        }

        @Override
        public void onAnimationEnd(Animator animator) {
            ConfigView.this.setEnabled(true);
        }

        @Override
        public void onAnimationCancel(Animator animator) {
        }

        @Override
        public void onAnimationRepeat(Animator animator) {
        }
    };
}