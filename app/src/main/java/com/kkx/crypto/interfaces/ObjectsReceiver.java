package com.kkx.crypto.interfaces;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 03/12/16
 */
public interface ObjectsReceiver {
    void onObjectsReceive(@IdRes int code, @NonNull Object... objects);
}