package com.kkx.crypto.activity;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.kkx.crypto.R;
import com.kkx.crypto.Utils;
import com.kkx.crypto.interfaces.ObjectsReceiver;
import com.kkx.crypto.view.ConfigView;
import com.kkx.library.cipher.Caesar;
import com.kkx.library.cipher.Changable;
import com.kkx.library.cipher.Cipher;
import com.kkx.library.cipher.Dictionary;
import com.kkx.library.cipher.Vernam;
import com.kkx.library.cipher.Vigenere;


/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 03/12/16
 */
public class MainActivity extends AppCompatActivity
        implements ObjectsReceiver, View.OnClickListener {

    private ConfigView mConfigView;
    private EditText mEditText;
    private TextView mTextView;

    private Cipher mCipher;

    private boolean mDecrypt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mConfigView = (ConfigView) findViewById(R.id.config_view);
        mEditText = (EditText) findViewById(R.id.et_input);
        mTextView = (TextView) findViewById(R.id.tv_result);

        mConfigView.setReceiver(this);
        mCipher = new Caesar(Dictionary.LATIN, 0);

        mEditText.addTextChangedListener(mTextWatcher);
        findViewById(R.id.btn_swap).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mDecrypt = !mDecrypt;
        item.setTitle(mDecrypt ? R.string.decrypt : R.string.encrypt);
        item.setIcon(mDecrypt ? R.drawable.svg_decrypt : R.drawable.svg_encrypt);
        update(mEditText.getText().toString());
        return super.onOptionsItemSelected(item);
    }

    private void update(@Nullable String text) {
        mTextView.setText(mDecrypt ? mCipher.decrypt(text) : mCipher.encrypt(text));
    }

    private void updateFilters() {
        final int keyLength = mConfigView.getKeyLength();
        mEditText.setFilters(new InputFilter[]{new LengthFilter(keyLength)});
        final String text = mEditText.getText().toString().trim();
        if (text.length() > keyLength) {
            mEditText.setText(text.substring(0, keyLength));
        }
    }

    @Override
    public void onClick(View view) {
        mEditText.setText(mTextView.getText().toString());
    }

    @Override
    public void onObjectsReceive(@IdRes int code, @NonNull Object... objects) {
        switch (code) {
            case R.id.code_checked_changed:
                final int groupId = (int) objects[0];
                final int buttonId = (int) objects[1];

                switch (groupId) {
                    case R.id.rg_cipher:
                        mCipher = (Cipher) objects[2];
                        if (mCipher instanceof Vernam) {
                            updateFilters();

                        } else {
                            mEditText.setFilters(new InputFilter[]{});
                        }
                        break;

                    case R.id.rg_dictionary:
                        if (mCipher instanceof Changable) {
                            ((Changable) mCipher).setDictionary(
                                    Dictionary.values()[Utils.getDictionaryIndex(buttonId)]
                            );
                        }
                        break;
                }
                break;

            case R.id.code_key_changed:
                if (mCipher instanceof Vigenere) {
                    ((Vigenere) mCipher).setKey((String) objects[0]);

                } else if (mCipher instanceof Vernam) {
                    ((Vernam) mCipher).setKey((String) objects[0]);
                    updateFilters();
                }
                break;

            case R.id.code_seekbar_changed:
                if (mCipher instanceof Caesar) {
                    ((Caesar) mCipher).setShift((int) objects[0]);
                }
                break;

            default:
                break;
        }
        update(mEditText.getText().toString());
    }

    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            update(editable.toString());
        }
    };
}