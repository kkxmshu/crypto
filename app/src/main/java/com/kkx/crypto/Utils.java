package com.kkx.crypto;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.kkx.crypto.interfaces.ObjectsReceiver;

import java.lang.ref.WeakReference;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 04.12.16
 */
public class Utils {

    public static boolean receiveObjects(@Nullable WeakReference<ObjectsReceiver> weakReceiver, @IdRes int code, @NonNull Object... objects) {
        final ObjectsReceiver receiver = weakReceiver == null ? null : weakReceiver.get();
        final boolean received = receiver != null;
        if (received) {
            receiver.onObjectsReceive(code, objects);
        }
        return received;
    }

    private static final int[] DICTIONARY_IDS = {R.id.rb_latin, R.id.rb_cyrillic, R.id.rb_mixed};

    public static int getDictionaryIndex(@IdRes int idRes) {
        for (int i = 0; i < DICTIONARY_IDS.length; i++) {
            if (idRes == DICTIONARY_IDS[i]) {
                return i;
            }
        }
        return 0;
    }

    public static void changeVisibility(int visibility, View... views) {
        for (View view : views) {
            view.setVisibility(visibility);
        }
    }

    public static String beautify(@NonNull Context context, @NonNull String third, int... ids) {
        final StringBuilder sb = new StringBuilder();
        for (int id : ids) {
            sb.append(context.getString(id)).append(" / ");
        }
        sb.append(third);
        return sb.toString();
    }

    public static String beautify(@NonNull String... strings) {
        final StringBuilder sb = new StringBuilder();
        for (String string : strings) {
            sb.append(string).append(" / ");
        }
        final String result = sb.toString();
        return result.substring(0, result.length() - 3);
    }
}
